# MetaGPT Role System Design

## Overview

The MetaGPT role system implements an AI-first approach to actor-based roles using protoactor-go. Each role is an independent actor with cognitive capabilities (Think-Act-React cycle) and communicates through message passing.

## Core Abstractions

### 1. Role Interface
```go
// Role represents an AI agent with actor capabilities
type Role interface {
    // Actor interface from protoactor-go
    actor.Actor
    
    // AI capabilities
    Think(ctx *actor.Context) (*Thoughts, error)
    Act(thoughts *Thoughts) (*Action, error)
    React(result *ActionResult) (*Reaction, error)
    Learn(reaction *Reaction) error
    
    // Role management
    GetCapabilities() []Capability
    AddCapability(cap Capability) error
    RemoveCapability(capID string) error
    
    // State management
    GetState() RoleState
    SetState(state RoleState) error
}

// RoleState represents the current state of a role
type RoleState struct {
    Status    RoleStatus
    Context   *RoleContext
    Resources *ResourceState
    Metrics   *RoleMetrics
}

// RoleStatus defines possible role states
type RoleStatus int

const (
    RoleInitialized RoleStatus = iota
    RoleReady
    RoleThinking
    RoleActing
    RoleReacting
    RoleLearning
    RolePaused
    RoleError
)
```

### 2. Base Role Implementation
```go
// BaseRole provides common role functionality
type BaseRole struct {
    // Actor components
    behavior actor.Behavior
    pid      *actor.PID
    context  *actor.Context
    
    // Role components
    state       RoleState
    memory      *RoleMemory
    capabilities []Capability
    metrics      *RoleMetrics
    
    // Configuration
    config *RoleConfig
}

// Initialize sets up the role
func (r *BaseRole) Initialize(config *RoleConfig) error {
    r.config = config
    r.state = RoleState{Status: RoleInitialized}
    r.memory = NewRoleMemory(config.Memory)
    r.capabilities = make([]Capability, 0)
    r.metrics = NewRoleMetrics()
    
    return nil
}

// Receive handles incoming messages
func (r *BaseRole) Receive(ctx actor.Context) {
    switch msg := ctx.Message().(type) {
    case *ThinkMessage:
        r.handleThinkMessage(ctx, msg)
    case *ActMessage:
        r.handleActMessage(ctx, msg)
    case *ReactMessage:
        r.handleReactMessage(ctx, msg)
    case *LearnMessage:
        r.handleLearnMessage(ctx, msg)
    case *actor.Started:
        r.handleStarted(ctx)
    case *actor.Stopped:
        r.handleStopped(ctx)
    default:
        r.handleUnknown(ctx, msg)
    }
}

// Think implements the thinking phase
func (r *BaseRole) Think(ctx *actor.Context) (*Thoughts, error) {
    // Update state
    r.state.Status = RoleThinking
    
    // Create thinking context
    thinkCtx := &ThinkContext{
        Memory:  r.memory,
        State:   r.state,
        Context: ctx,
    }
    
    // Execute thinking process
    thoughts, err := r.executeThinking(thinkCtx)
    if err != nil {
        r.state.Status = RoleError
        return nil, err
    }
    
    return thoughts, nil
}

// Act implements the action phase
func (r *BaseRole) Act(thoughts *Thoughts) (*Action, error) {
    // Update state
    r.state.Status = RoleActing
    
    // Create action context
    actCtx := &ActionContext{
        Thoughts: thoughts,
        Memory:   r.memory,
        State:    r.state,
    }
    
    // Execute action
    action, err := r.executeAction(actCtx)
    if err != nil {
        r.state.Status = RoleError
        return nil, err
    }
    
    return action, nil
}

// React implements the reaction phase
func (r *BaseRole) React(result *ActionResult) (*Reaction, error) {
    // Update state
    r.state.Status = RoleReacting
    
    // Create reaction context
    reactCtx := &ReactionContext{
        Result:  result,
        Memory:  r.memory,
        State:   r.state,
    }
    
    // Execute reaction
    reaction, err := r.executeReaction(reactCtx)
    if err != nil {
        r.state.Status = RoleError
        return nil, err
    }
    
    return reaction, nil
}
```

### 3. Role Memory
```go
// RoleMemory manages role-specific memory
type RoleMemory struct {
    // Memory components
    working   *WorkingMemory
    episodic  *EpisodicMemory
    semantic  *SemanticMemory
    
    // Memory management
    capacity  int
    policy    *MemoryPolicy
}

// Store stores information in appropriate memory
func (m *RoleMemory) Store(ctx context.Context, item *MemoryItem) error {
    switch item.Type {
    case WorkingMemoryType:
        return m.working.Store(ctx, item)
    case EpisodicMemoryType:
        return m.episodic.Store(ctx, item)
    case SemanticMemoryType:
        return m.semantic.Store(ctx, item)
    default:
        return ErrUnknownMemoryType
    }
}

// Recall retrieves information from memory
func (m *RoleMemory) Recall(ctx context.Context, query *MemoryQuery) ([]*MemoryItem, error) {
    var results []*MemoryItem
    
    // Search in working memory
    if query.SearchWorking {
        items, err := m.working.Search(ctx, query)
        if err != nil {
            return nil, err
        }
        results = append(results, items...)
    }
    
    // Search in episodic memory
    if query.SearchEpisodic {
        items, err := m.episodic.Search(ctx, query)
        if err != nil {
            return nil, err
        }
        results = append(results, items...)
    }
    
    // Search in semantic memory
    if query.SearchSemantic {
        items, err := m.semantic.Search(ctx, query)
        if err != nil {
            return nil, err
        }
        results = append(results, items...)
    }
    
    return results, nil
}
```

## Predefined Roles

### 1. Product Manager
```go
// ProductManager role
type ProductManager struct {
    BaseRole
    requirements *RequirementsList
    vision       *ProductVision
    planning     *PlanningState
}

func (pm *ProductManager) Think(ctx *actor.Context) (*Thoughts, error) {
    // Analyze requirements
    reqs, err := pm.AnalyzeRequirements(ctx)
    if err != nil {
        return nil, err
    }
    
    // Plan roadmap
    roadmap, err := pm.PlanRoadmap(reqs)
    if err != nil {
        return nil, err
    }
    
    // Prioritize features
    priorities, err := pm.PrioritizeFeatures(roadmap)
    if err != nil {
        return nil, err
    }
    
    return &Thoughts{
        Requirements: reqs,
        Roadmap:     roadmap,
        Priorities:  priorities,
    }, nil
}
```

### 2. Architect
```go
// Architect role
type Architect struct {
    BaseRole
    design    *DesignState
    techStack *TechStack
    patterns  *Patterns
}

func (a *Architect) Think(ctx *actor.Context) (*Thoughts, error) {
    // Analyze requirements
    analysis, err := a.AnalyzeRequirements(ctx)
    if err != nil {
        return nil, err
    }
    
    // Design architecture
    design, err := a.DesignArchitecture(analysis)
    if err != nil {
        return nil, err
    }
    
    // Evaluate design
    evaluation, err := a.EvaluateDesign(design)
    if err != nil {
        return nil, err
    }
    
    return &Thoughts{
        Analysis:    analysis,
        Design:     design,
        Evaluation: evaluation,
    }, nil
}
```

### 3. Engineer
```go
// Engineer role
type Engineer struct {
    BaseRole
    coding *CodingState
    skills *TechnicalSkills
    tools  *DevelopmentTools
}

func (e *Engineer) Think(ctx *actor.Context) (*Thoughts, error) {
    // Analyze task
    analysis, err := e.AnalyzeTask(ctx)
    if err != nil {
        return nil, err
    }
    
    // Plan implementation
    plan, err := e.PlanImplementation(analysis)
    if err != nil {
        return nil, err
    }
    
    // Design solution
    design, err := e.DesignSolution(plan)
    if err != nil {
        return nil, err
    }
    
    return &Thoughts{
        Analysis: analysis,
        Plan:     plan,
        Design:   design,
    }, nil
}
```

## Role Communication

### 1. Message Types
```go
// Message types for role communication
type MessageType int

const (
    // Core messages
    ThinkMsg MessageType = iota
    ActMsg
    ReactMsg
    LearnMsg
    
    // Role-specific messages
    RequirementMsg
    DesignMsg
    CodeMsg
    ReviewMsg
)

// Message structure
type Message struct {
    Type     MessageType
    Content  interface{}
    Sender   *actor.PID
    Receiver *actor.PID
    Time     time.Time
    Context  *MessageContext
}
```

### 2. Message Routing
```go
// MessageRouter handles message routing between roles
type MessageRouter struct {
    routes map[MessageType][]actor.PID
    mu     sync.RWMutex
}

func (mr *MessageRouter) Route(msg *Message) []actor.PID {
    mr.mu.RLock()
    defer mr.mu.RUnlock()
    return mr.routes[msg.Type]
}

func (mr *MessageRouter) Register(msgType MessageType, pid *actor.PID) {
    mr.mu.Lock()
    defer mr.mu.Unlock()
    mr.routes[msgType] = append(mr.routes[msgType], pid)
}
```

## Role Management

### 1. Role Factory
```go
// RoleFactory creates new roles
type RoleFactory struct {
    templates map[string]*RoleTemplate
    config    *RoleConfig
}

func (rf *RoleFactory) CreateRole(roleType string) (Role, error) {
    template, ok := rf.templates[roleType]
    if !ok {
        return nil, ErrUnknownRoleType
    }
    
    role := template.CreateRole()
    if err := role.Initialize(rf.config); err != nil {
        return nil, err
    }
    
    return role, nil
}
```

### 2. Role Supervisor
```go
// RoleSupervisor manages role lifecycle
type RoleSupervisor struct {
    actor.Actor
    roles    map[*actor.PID]Role
    strategy SupervisionStrategy
}

func (s *RoleSupervisor) HandleFailure(ctx actor.Context, pid *actor.PID, err error) {
    switch s.strategy {
    case OneForOne:
        s.restartRole(ctx, pid)
    case OneForAll:
        s.restartAllRoles(ctx)
    case RestForOne:
        s.restartDependentRoles(ctx, pid)
    }
}
```

## Testing

### 1. Role Testing
```go
func TestRoleThinkActReact(t *testing.T) {
    // Setup test environment
    ctx := actor.NewContext()
    role := NewTestRole()
    
    // Test Think phase
    thoughts, err := role.Think(ctx)
    assert.NoError(t, err)
    assert.NotNil(t, thoughts)
    
    // Test Act phase
    action, err := role.Act(thoughts)
    assert.NoError(t, err)
    assert.NotNil(t, action)
    
    // Test React phase
    reaction, err := role.React(action.Result)
    assert.NoError(t, err)
    assert.NotNil(t, reaction)
}
```

### 2. Integration Testing
```go
func TestRoleCollaboration(t *testing.T) {
    // Setup test system
    system := NewTestSystem()
    
    // Create roles
    pm := system.CreateRole("product_manager")
    architect := system.CreateRole("architect")
    engineer := system.CreateRole("engineer")
    
    // Test collaboration
    req := &RequirementMessage{
        Content: "Create login system",
    }
    
    // Send requirement to PM
    result, err := system.SendMessage(pm.PID(), req)
    assert.NoError(t, err)
    assert.NotNil(t, result)
    
    // Verify collaboration flow
    assert.True(t, result.FlowCompleted)
    assert.NotEmpty(t, result.Artifacts)
} 