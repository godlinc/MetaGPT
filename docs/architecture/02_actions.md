# MetaGPT Action System Design

## Overview

The Action system implements the execution layer of MetaGPT, integrating with protoactor-go to provide a distributed, message-driven action execution framework. Actions represent concrete operations that roles can perform, with built-in support for validation, execution, and monitoring.

## Core Concepts

### 1. Action Model
```
+------------------------+
|        Action         |
|------------------------|
|  +----------------+   |
|  |   Context     |   |
|  +----------------+   |
|         ↓            |
|  +----------------+   |
|  |   Execution   |   |
|  +----------------+   |
|         ↓            |
|  +----------------+   |
|  |   Result      |   |
|  +----------------+   |
+------------------------+
```

### 2. Action Lifecycle
1. Initialization (Context Setup)
2. Validation (Pre-execution)
3. Execution (Core Operation)
4. Result Processing (Post-execution)
5. Feedback (Learning)

## Core Components

### 1. Action Interface
```go
// Action represents a concrete operation
type Action interface {
    // Core operations
    Execute(ctx context.Context) (*ActionResult, error)
    Validate() error
    Rollback() error
    
    // Metadata
    GetID() string
    GetType() ActionType
    GetStatus() ActionStatus
    
    // Actor integration
    GetPID() *actor.PID
    SetPID(pid *actor.PID)
}

// ActionStatus represents the current state of an action
type ActionStatus int

const (
    ActionCreated ActionStatus = iota
    ActionValidated
    ActionRunning
    ActionCompleted
    ActionFailed
    ActionRolledBack
)

// ActionType categorizes different types of actions
type ActionType int

const (
    CognitiveAction ActionType = iota
    ExecutiveAction
    CommunicativeAction
    LearningAction
)
```

### 2. Base Action Implementation
```go
// BaseAction provides common action functionality
type BaseAction struct {
    id       string
    actionType ActionType
    status   ActionStatus
    context  *ActionContext
    result   *ActionResult
    pid      *actor.PID
    
    // Actor components
    behavior actor.Behavior
    mailbox  *actor.Mailbox
}

// Execute implements the core execution logic
func (a *BaseAction) Execute(ctx context.Context) (*ActionResult, error) {
    // Validate prerequisites
    if err := a.Validate(); err != nil {
        return nil, err
    }
    
    // Update status
    a.status = ActionRunning
    
    // Execute action logic
    result, err := a.executeLogic(ctx)
    if err != nil {
        a.status = ActionFailed
        return nil, err
    }
    
    // Update status and result
    a.status = ActionCompleted
    a.result = result
    
    return result, nil
}

// Rollback handles execution failure
func (a *BaseAction) Rollback() error {
    // Update status
    a.status = ActionRolledBack
    
    // Implement rollback logic
    if err := a.rollbackLogic(); err != nil {
        return err
    }
    
    return nil
}
```

### 3. Action Context
```go
// ActionContext provides execution context
type ActionContext struct {
    // Execution context
    params      map[string]interface{}
    constraints *Constraints
    resources   *Resources
    
    // Actor context
    actorCtx    actor.Context
    sender      *actor.PID
    supervisor  *actor.PID
    
    // Monitoring
    metrics     *ActionMetrics
    logger      *ActionLogger
}

// NewActionContext creates a new action context
func NewActionContext(actorCtx actor.Context) *ActionContext {
    return &ActionContext{
        params:     make(map[string]interface{}),
        actorCtx:   actorCtx,
        sender:     actorCtx.Sender(),
        supervisor: actorCtx.Parent(),
        metrics:    NewActionMetrics(),
        logger:     NewActionLogger(),
    }
}
```

## Action Types

### 1. Cognitive Actions
```go
// AnalyzeAction implements cognitive processing
type AnalyzeAction struct {
    BaseAction
    analyzer  *Analyzer
    input     *AnalysisInput
    output    *AnalysisOutput
}

func (a *AnalyzeAction) executeLogic(ctx context.Context) (*ActionResult, error) {
    // Perform analysis
    analysis, err := a.analyzer.Analyze(a.input)
    if err != nil {
        return nil, err
    }
    
    // Process results
    output, err := a.processAnalysis(analysis)
    if err != nil {
        return nil, err
    }
    
    return &ActionResult{
        Output: output,
        Status: ActionCompleted,
    }, nil
}
```

### 2. Executive Actions
```go
// WriteCodeAction implements code generation
type WriteCodeAction struct {
    BaseAction
    spec      *CodeSpec
    generator *CodeGenerator
    validator *CodeValidator
}

func (a *WriteCodeAction) executeLogic(ctx context.Context) (*ActionResult, error) {
    // Generate code
    code, err := a.generator.Generate(a.spec)
    if err != nil {
        return nil, err
    }
    
    // Validate code
    if err := a.validator.Validate(code); err != nil {
        return nil, err
    }
    
    return &ActionResult{
        Output: code,
        Status: ActionCompleted,
    }, nil
}
```

### 3. Communication Actions
```go
// SendMessageAction implements message sending
type SendMessageAction struct {
    BaseAction
    message  *Message
    targets  []*actor.PID
    router   *MessageRouter
}

func (a *SendMessageAction) executeLogic(ctx context.Context) (*ActionResult, error) {
    // Route message to targets
    for _, target := range a.targets {
        if err := a.router.Route(a.message, target); err != nil {
            return nil, err
        }
    }
    
    return &ActionResult{
        Output: &MessageDeliveryStatus{
            Delivered: true,
            Time:     time.Now(),
        },
        Status: ActionCompleted,
    }, nil
}
```

## Action Execution

### 1. Action Executor
```go
// ActionExecutor manages action execution
type ActionExecutor struct {
    system     *actor.ActorSystem
    scheduler  *ActionScheduler
    monitor    *ActionMonitor
    registry   *ActionRegistry
}

func (e *ActionExecutor) ExecuteAction(action Action) (*ActionResult, error) {
    // Register action
    if err := e.registry.Register(action); err != nil {
        return nil, err
    }
    
    // Schedule execution
    if err := e.scheduler.Schedule(action); err != nil {
        return nil, err
    }
    
    // Monitor execution
    e.monitor.StartMonitoring(action)
    
    // Execute action
    result, err := action.Execute(context.Background())
    
    // Update monitoring
    e.monitor.StopMonitoring(action)
    
    return result, err
}
```

### 2. Action Scheduler
```go
// ActionScheduler handles action scheduling
type ActionScheduler struct {
    queue     *ActionQueue
    executor  *ActionExecutor
    strategy  SchedulingStrategy
}

func (s *ActionScheduler) Schedule(action Action) error {
    // Apply scheduling strategy
    priority := s.strategy.CalculatePriority(action)
    
    // Add to queue
    return s.queue.Add(action, priority)
}

func (s *ActionScheduler) processQueue() {
    for {
        // Get next action
        action := s.queue.Next()
        if action == nil {
            continue
        }
        
        // Execute action
        go s.executor.ExecuteAction(action)
    }
}
```

## Action Composition

### 1. Action Chain
```go
// ActionChain represents sequential actions
type ActionChain struct {
    BaseAction
    actions []Action
    context *ChainContext
}

func (c *ActionChain) Execute(ctx context.Context) (*ActionResult, error) {
    var results []*ActionResult
    
    // Execute actions sequentially
    for _, action := range c.actions {
        result, err := action.Execute(ctx)
        if err != nil {
            return nil, err
        }
        results = append(results, result)
    }
    
    return &ActionResult{
        Output:  results,
        Status:  ActionCompleted,
    }, nil
}
```

### 2. Action Pipeline
```go
// ActionPipeline represents parallel actions
type ActionPipeline struct {
    BaseAction
    stages  [][]Action
    context *PipelineContext
}

func (p *ActionPipeline) Execute(ctx context.Context) (*ActionResult, error) {
    var results [][]*ActionResult
    
    // Execute stages in sequence
    for _, stage := range p.stages {
        // Execute actions in parallel
        stageResults, err := p.executeStage(ctx, stage)
        if err != nil {
            return nil, err
        }
        results = append(results, stageResults)
    }
    
    return &ActionResult{
        Output:  results,
        Status:  ActionCompleted,
    }, nil
}
```

## Action Management

### 1. Action Registry
```go
// ActionRegistry manages action registration
type ActionRegistry struct {
    actions map[string]Action
    mu      sync.RWMutex
}

func (r *ActionRegistry) Register(action Action) error {
    r.mu.Lock()
    defer r.mu.Unlock()
    
    r.actions[action.GetID()] = action
    return nil
}

func (r *ActionRegistry) Get(id string) (Action, error) {
    r.mu.RLock()
    defer r.mu.RUnlock()
    
    action, ok := r.actions[id]
    if !ok {
        return nil, ErrActionNotFound
    }
    
    return action, nil
}
```

### 2. Action Monitor
```go
// ActionMonitor tracks action execution
type ActionMonitor struct {
    metrics  *ActionMetrics
    logger   *ActionLogger
    alerts   *AlertManager
}

func (m *ActionMonitor) StartMonitoring(action Action) {
    // Start metrics collection
    m.metrics.StartTracking(action)
    
    // Initialize logging
    m.logger.StartLogging(action)
    
    // Setup alerts
    m.alerts.SetupAlerts(action)
}

func (m *ActionMonitor) StopMonitoring(action Action) {
    // Stop metrics collection
    m.metrics.StopTracking(action)
    
    // Finalize logging
    m.logger.StopLogging(action)
    
    // Clear alerts
    m.alerts.ClearAlerts(action)
}
```

## Testing

### 1. Action Testing
```go
func TestActionExecution(t *testing.T) {
    // Setup test environment
    ctx := context.Background()
    action := NewTestAction()
    
    // Execute action
    result, err := action.Execute(ctx)
    assert.NoError(t, err)
    assert.NotNil(t, result)
    
    // Verify result
    assert.Equal(t, ActionCompleted, result.Status)
    assert.NotNil(t, result.Output)
}
```

### 2. Integration Testing
```go
func TestActionPipeline(t *testing.T) {
    // Setup test pipeline
    pipeline := NewActionPipeline()
    
    // Add test actions
    pipeline.AddStage([]Action{
        NewTestAction1(),
        NewTestAction2(),
    })
    
    // Execute pipeline
    result, err := pipeline.Execute(context.Background())
    assert.NoError(t, err)
    assert.NotNil(t, result)
    
    // Verify results
    assert.Equal(t, ActionCompleted, result.Status)
    assert.Len(t, result.Output, 2)
}
```

## Security

### 1. Action Security
```python
class ActionSecurity:
    """Action security management"""
    
    def validate_permissions(self, action: Action, role: PID) -> bool:
        """Validate execution permissions"""
        pass
        
    def audit_execution(self, action: Action) -> AuditLog:
        """Audit action execution"""
        pass
        
    def secure_resources(self, action: Action) -> None:
        """Secure action resources"""
        pass
```

### 2. Resource Protection
```python
class ResourceProtection:
    """Resource access protection"""
    
    def check_access(self, resource: Resource, action: Action) -> bool:
        """Check resource access"""
        pass
        
    def lock_resource(self, resource: Resource) -> None:
        """Lock resource for exclusive access"""
        pass
        
    def release_resource(self, resource: Resource) -> None:
        """Release resource lock"""
        pass
``` 